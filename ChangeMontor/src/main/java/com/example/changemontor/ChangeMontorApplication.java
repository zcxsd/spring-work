package com.example.changemontor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChangeMontorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChangeMontorApplication.class, args);
    }

}
