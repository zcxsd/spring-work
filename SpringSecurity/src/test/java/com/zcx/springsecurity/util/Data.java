package com.zcx.springsecurity.util;

/**
 * @Author:
 * @DATE:2023/5/25 11:54
 * @Description:
 * @Version 1.0
 */
public enum Data {
    TIF(0, "TIF"),
    SHP(1, "SHP"),
    TD(2, "TD"),
    MULTI(3, "MULTI");

    private final int label;
    private final String text;

    private Data(int label, String text) {
        this.label = label;
        this.text = text;
    }

    public int getLabel() {
        return label;
    }

    public String getText() {
        return text;
    }
}
