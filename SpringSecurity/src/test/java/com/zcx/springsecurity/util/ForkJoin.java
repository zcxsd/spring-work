package com.zcx.springsecurity.util;

import org.junit.jupiter.params.shadow.com.univocity.parsers.common.record.Record;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @Author:
 * @DATE:2023/5/17 11:49
 * @Description:
 * @Version 1.0
 */
public class ForkJoin {
    private static final Integer MAX = 200;

    static class MyForkJoinTask extends RecursiveTask<Integer> {
        // 子任务开始计算的值
        private Integer startValue;
        // 子任务结束计算的值
        private Integer endValue;

        public MyForkJoinTask(Integer startValue, Integer endValue) {
            this.startValue = startValue;
            this.endValue = endValue;
        }

        @Override
        protected Integer compute() {
// 如果条件成立，说明这个任务所需要计算的数值分为足够小了
// 可以正式进行累加计算了
            if (endValue - startValue < MAX) {
                System.out.println("开始计算的部分：startValue = " + startValue
                        + ";endValue = " + endValue);
                Integer totalValue = 0;
                for (int index = this.startValue; index <= this.endValue;
                     index++) {
                    totalValue += index;
                }
                return totalValue;
            }
// 否则再进行任务拆分，拆分成两个任务
            else {
                MyForkJoinTask subTask1 = new MyForkJoinTask(startValue,
                        (startValue + endValue) / 2);
                subTask1.fork();
                MyForkJoinTask subTask2 = new MyForkJoinTask((startValue +
                        endValue) / 2 + 1, endValue);
                subTask2.fork();
                return subTask1.join() + subTask2.join();
            }
        }
    }

    static class MyListForktask extends RecursiveTask<List<Record>> {
        private List<Record> list;

        public MyListForktask(List<Record> list) {
            this.list = list;
        }

        @Override
        protected List<Record> compute() {
            List<Record> recordList = new ArrayList<>();
            if (list.size() < MAX) {
                list.stream().filter(s -> s.getString("").equals("")).forEach(recordList::add);
            } else {
                List<Record> partitionedList = Stream.iterate(0, i -> i + MAX)
                        .limit((list.size() + MAX - 1) / MAX)
                        .map(j -> list.subList(j, Math.min(j + MAX, list.size())))
                        .flatMap(List::stream).collect(Collectors.toList());
                MyListForktask myListForktask = new MyListForktask(partitionedList);
                myListForktask.fork();
                return myListForktask.join();
            }
            return recordList;
        }
    }


    public static void main(String[] args) {


// 这是Fork/Join框架的线程池
        ForkJoinPool pool = new ForkJoinPool();
        List<String> img = new ArrayList<>();


        pool.submit(() -> {
            if (img.size() <= 3) {

            } else {

            }
        }).join();


        MyForkJoinTask myForkJoinTask = new MyForkJoinTask(1, 1001);
        ForkJoinTask<Integer> taskFuture = pool.submit(myForkJoinTask);
        try {
            Integer result = taskFuture.get();
            System.out.println("result = " + result);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace(System.out);
        }
    }

}
