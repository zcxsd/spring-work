package com.zcx.springsecurity.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author:
 * @DATE:2023/6/7 10:57
 * @Description:
 * @Version 1.0
 */
@RestController
public class Hello {
    @RequestMapping("/hello")
    public String hello() {
        System.out.println("security");
        return "security";
    }
}
