package com.zcx.pdf_word;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdfWordApplication {

    public static void main(String[] args) {
        SpringApplication.run(PdfWordApplication.class, args);
    }

}
