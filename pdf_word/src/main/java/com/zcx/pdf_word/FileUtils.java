package com.zcx.pdf_word;
import com.aspose.pdf.Document;
import com.aspose.pdf.SaveFormat;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.graphics.PdfMargins;

import java.awt.geom.Point2D;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import java.io.FileOutputStream;

/**
 * @Author:\
 * @DATE:2023/4/11 16:40
 * @Description:
 * @Version 1.0
 */
public class FileUtils {
    public static void main(String[] args) throws IOException {
         pdf2doc("G:\\XQ\\r\\SpringCloud.pdf");
       // pdfcf("G:\\XQ\\r\\SpringCloud.pdf");
        //Map<Integer,Integer> map=new HashMap<>();
        //map.put(1,3);
        //pdf2cf("",map);
    }
    //pdf转doc
    public static void pdf2doc(String pdfPath) {
        long old = System.currentTimeMillis();
        try {
            //新建一个word文档
            String wordPath=pdfPath.substring(0,pdfPath.lastIndexOf("."))+".docx";
            FileOutputStream os = new FileOutputStream(wordPath);
            //doc是将要被转化的word文档
            Document doc = new Document(pdfPath);
            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(os, SaveFormat.DocX);
            os.close();
            //转化用时
            long now = System.currentTimeMillis();
            System.out.println("Pdf 转 Word 共耗时：" + ((now - old) / 1000.0) + "秒");
        } catch (Exception e) {
            System.out.println("Pdf 转 Word 失败...");
            e.printStackTrace();
        }
    }
    public static void pdfcf(String pdfpath){
        PdfDocument document=new PdfDocument();
        document.loadFromFile(pdfpath);
        document.split("G:\\BaiduNetdiskDownload\\java书籍\\java\\cs\\sp-{0}.pdf",0);
        document.close();
    }
    public static void pdf2cf(String pdfpath, Map<Integer,Integer> map){
        //加载PDF文档
        PdfDocument doc = new PdfDocument();
        doc.loadFromFile("sample.pdf");
        //获取pdf页数
        int pagenumber=doc.getPages().getCount();
        //新建一个PDF文档
        PdfDocument newDoc1 = new PdfDocument();

        PdfPageBase page;

        //将原PDF文档的第1、2页添加至新建的PDF
        for(int i = 0;i<2;i++)
        {
            page = newDoc1.getPages().add(doc.getPages().get(i).getSize(), new PdfMargins(0));
            doc.getPages().get(i).createTemplate().draw(page, new Point2D.Float(0,0));
        }
        //保存文档
        newDoc1.saveToFile("split/Doc1.pdf");
        //新建另一个PDF文档
        PdfDocument newDoc2 = new PdfDocument();
        //将原PDF文档的第3至5页添加至新建的PDF
        for(int i = 2;i<5;i++)
        {
            page = newDoc2.getPages().add(doc.getPages().get(i).getSize(), new PdfMargins(0));
            doc.getPages().get(i).createTemplate().draw(page, new Point2D.Float(0,0));
        }
        //保存文档
        newDoc2.saveToFile("split/Doc2.pdf");
    }

}
