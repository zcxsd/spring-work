package com.zcx.redis.common.base;

import com.zcx.redis.bean.common.PageBean;
import com.by.utils.MapUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Service
public abstract class BaseServiceImpl<T,M extends BaseMapper> implements BaseService<T>{


    @Autowired(required = false)
     M mapper;

    @Override
    public int save(T mapOrEntiry) {
        return mapper.save(mapOrEntiry);
    }

    @Override
    public int update(T mapOrEntiry) {
        return mapper.update(mapOrEntiry);
    }

    @Override
    public int delete(Serializable id) {
        return mapper.delete(id);
    }

    @Override
    public T get(Serializable id) {
        return (T) mapper.get(id);
    }

    @Override
    public PageBean query(Map map) {
        PageHelper.startPage(MapUtils.getPageNumValue(map),MapUtils.getPageSizeValue(map));
        List list=mapper.query(map);
        Page page= (Page) list;
        return new PageBean(page.getPageNum(),page.getPageSize(),page.getTotal(),page.getPages(),list);
    }
}
