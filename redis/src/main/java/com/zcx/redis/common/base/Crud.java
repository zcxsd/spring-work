package com.zcx.redis.common.base;


import java.io.Serializable;

/**
 * 抽象出来 增删改查 基本接口
 */
public interface Crud<T> {

    /**
     * 保存功能
     * @param mapOrEntiry  要保存的map或javabean
     * @return 保存的结果  1 成功  0失败
     */

    int save(T mapOrEntiry);

    /**
     * 修改功能
     * @param mapOrEntiry 要修改的map或javabean
     * @return 修改的结果  1 成功  0失败
     */

    int update(T mapOrEntiry);

    /**
     * 删除指定的数据
     * @param id  要删除数据的id
     * @return  删除结果  1成功 0 失败
     */

    int delete(Serializable id);

    /**
     * 根据id查询结果
     * @param id 要查询的id
     * @return
     */

    T get(Serializable id);

}
