package com.zcx.redis.common.base;

import java.util.List;
import java.util.Map;

/**
 * 抽象出来所有的 通用的mapper操作
 */
public interface BaseMapper<T> extends Crud<T> {
    /**
     * 查询指定页的记录
     * @param map 要查询的条件
     * @return  指定页的记录列表
     */
    List<Map> query(Map map);
}
