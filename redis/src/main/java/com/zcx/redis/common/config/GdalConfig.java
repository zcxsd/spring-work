package com.zcx.redis.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @Author:
 * @DATE:2023/4/13 16:03
 * @Description:
 * @Version 1.0
 */
@Configuration
public class GdalConfig {
    @Value("${GDAL.gdalData}")
    private String gdalData;
    @Value("${GDAL.proj_lib}")
    private String proj_lib;
    @Value("${GDAL.gdalpath}")
    private String gadlpath;
    @Value("${GDAL.gdalplugins}")
    private String gdalplugins;
        @PostConstruct
        public void init() {
            System.setProperty("GDAL_DRIVER_PATH",gdalplugins);
            System.setProperty("GDAL_LIBRARY_PATH",gadlpath);
            System.setProperty("GDAL_DATA", gdalData);
            System.setProperty("PROJ_LIB",proj_lib);
        }

}
