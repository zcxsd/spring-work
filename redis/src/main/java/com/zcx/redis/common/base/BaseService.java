package com.zcx.redis.common.base;


import com.zcx.redis.bean.common.PageBean;

import java.util.Map;

/**
 * 抽象出来所有 通用的 业务方法
 */
public interface BaseService<T> extends Crud<T> {

    /**
     * 分页查询
     * @param map 查询所需要的条件
     * @return  分页结果  ，含有第几页、每页记录数、总记录数、总页数、当前页记录数
     */
    PageBean query(Map map);
}
