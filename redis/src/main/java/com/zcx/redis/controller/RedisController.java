package com.zcx.redis.controller;

import com.zcx.redis.bean.common.ResultBean;
import com.zcx.redis.controller.common.BaseController;

//import com.zcx.redis.utils.JsonShp;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconst;
import org.gdal.ogr.DataSource;
import org.gdal.ogr.ogr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author:
 * @DATE:2023/4/13 15:06
 * @Description:
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/redis")
@Api(tags = "redis管理")
@Slf4j
public class RedisController extends BaseController {
    @Autowired
    RedisTemplate redisTemplate;
    @RequestMapping("/msg")
    public ResultBean m1(){
        redisTemplate.opsForValue().set("zcx","1");
        redisTemplate.delete("zcx");

//         redisCache.setCacheObject("zcx",1);
        return  success(redisTemplate.opsForValue().get("zcx"));
    }
    @RequestMapping("/gdal")
    public ResultBean gdal(String url){
        gdal.AllRegister();
       // String url = "G:\\XQ\\peizhun\\1-3\\JL1GF03B01_PMS_20210328101035_200045489_101_0003_001_L1_MSS.tif";
        Dataset dataset = gdal.Open(url, gdalconst.GA_ReadOnly);

        if (dataset == null) {
            return fail("数据错误");
//            throw new RuntimeException("Could not open " + url);
        }
        int width = dataset.getRasterXSize();
        int height = dataset.getRasterYSize();
        int bands = dataset.getRasterCount();
        Map<String,Integer> m=new HashMap<>();
        m.put("宽度",width);
        m.put("高度",height);
        m.put("波段数",bands);
        System.out.println(m);
        return success(m);
    }
    @RequestMapping("/gdal2")
    public ResultBean gdal2(String url) throws InterruptedException {
             boolean flag=true;
            // 注册所有的驱动  
            ogr.RegisterAll();
            // 为了支持中文路径，请添加下面这句代码  
            gdal.SetConfigOption("GDAL_FILENAME_IS_UTF8", "YES");
            // 为了使属性表字段支持中文，请添加下面这句 
            gdal.SetConfigOption("SHAPE_ENCODING", "");

            String strVectorFile = "G:\\XQ\\jinzita\\caiqiemian\\caiqiemian\\caiqiemian.shp";
            //打开数据  
            DataSource ds = ogr.Open(strVectorFile, 0);
            if (ds == null) {
                System.out.println("打开文件失败！");

            }
            System.out.println("打开文件成功！");
            // GeoJSON shp转json的驱动
            // 面的记录  ESRI Shapefile
            // 线的记录  ESRI Shapefile
            // 点的记录  ESRI Shapefile
            String strDriverName = "ESRI Shapefile";
            org.gdal.ogr.Driver dv = ogr.GetDriverByName(strDriverName);
            if (dv == null) {
                System.out.println("打开驱动失败！");

            }
            System.out.println("打开驱动成功！");
            dv.CopyDataSource(ds, "G:\\XQ\\jinzita\\caiqiemian\\caiqiemian\\caiqiemian3333.shp");
            if (!flag) {
                Thread.sleep(5000 * 2);
                System.out.println("转换成功！");
                System.out.println("转换成功！");
                System.out.println("转换成功！");
            }


//        public static void main(String[] args) throws InterruptedException {
//            shp("G:\\XQ\\jinzita\\caiqiemian\\caiqiemian\\caiqiemian.shp","G:\\XQ\\jinzita\\caiqiemian\\caiqiemian\\caiqiemian11111.shp",true);
//        }
        return success("成功");
    }

    @RequestMapping("/shp")
    public ResultBean shp(String url) throws IOException {
        System.out.println(url);
        File f=new File(url);
        String pro="";
        if (f.exists()){
//             pro= JsonShp.shp2geoJson(url);
        }
        return success(pro);

    }

}
