package com.zcx.redis.controller.common;

import com.zcx.redis.bean.common.ResultBean;

public abstract class BaseController {

    private final int SUCCESS=200;
    private final int FAIL=0;

    /**
     * 处理成功
     * @param msg 成功的描述消息
     * @return
     */
    protected ResultBean success(String msg){
        return new ResultBean(SUCCESS,msg,null);
    }

    /**
     * 处理成功
     * @param msg 成功的描述消息
     * @param data  操作的结果
     * @return
     */
    protected ResultBean success(String msg,Object data){
        return new ResultBean(SUCCESS,msg,data);
    }

    /**
     * 处理成功
     * @param data  操作的结果
     * @return
     */
    protected ResultBean success(Object data){
        return new ResultBean(SUCCESS,"",data);
    }

    /**
     * 处理失败
     * @param msg 错误消息描述
     * @return
     */
    protected ResultBean fail(String msg){
        return new ResultBean(FAIL,msg,null);
    }

    /**
     * 处理失败
     * @param msg 错误消息描述
     * @param error 具体数据
     * @return
     */
    protected ResultBean fail(String msg,Object error){
        return new ResultBean(FAIL,msg,error);
    }


}
