package com.zcx.redis.bean.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页结果
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBean {
    int pageNum;  //当前第几页
    int pageSize; //每页显示的记录数
    long total; //总记录数
    int pages; //总页数
    List list;  //分页列表
}
