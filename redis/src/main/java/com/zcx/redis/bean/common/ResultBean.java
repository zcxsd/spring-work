package com.zcx.redis.bean.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 前后端交互的数据统一格式
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ResultBean",description = "后台接口返回数据")
public class ResultBean {

    @ApiModelProperty(value = "接口处理结果状态码")
    int code;  //状态码
    @ApiModelProperty(value = "接口处理结果消息描述")
    String msg; //结果消息描述
    @ApiModelProperty(value = "接口处理的结果")
    Object data; //封装的结果
}
