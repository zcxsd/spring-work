package com.zcx.redis.bean.sys;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * @Author:zcx
 * @DATE:2023/1/17 9:07
 * @Description:
 * @Version 1.0
 */
@Data
@ApiModel(value = "Exe",description = "保存用户时传递的参数")
public class Exe {
     Integer id;
     String name;
     String path;
     Date time;
     String remark;
     Integer status;
     Integer task_type;
     Integer type;
     Integer order_by;
     Integer del;
}
