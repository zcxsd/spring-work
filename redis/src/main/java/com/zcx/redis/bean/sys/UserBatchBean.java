package com.zcx.redis.bean.sys;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * description:
 * author:zcx
 * lenovo
 * 时间：2022.05.16.08.07
 */
@Data
public class UserBatchBean {

    @NotBlank(message = "操作命令不能为空！")
    String command;

    @NotNull(message = "批量操作ids不能为空")
    @Size(message = "批量操作ids至少有一条数据",min = 1)
    String [] ids;
}
