//package com.zcx.redis.utils;
//
//
//import org.geotools.data.shapefile.ShapefileDataStore;
//import org.geotools.data.shapefile.ShapefileDataStoreFactory;
//import org.geotools.data.store.ContentFeatureSource;
//import org.geotools.feature.FeatureCollection;
//import org.geotools.feature.FeatureIterator;
//import org.geotools.geojson.feature.FeatureJSON;
//import org.opengis.feature.simple.SimpleFeature;
//
//import java.io.*;
//import java.nio.charset.Charset;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @Author:
// * @DATE:2023/4/14 10:30
// * @Description:
// * @Version 1.0
// */
//public class JsonShp {
//    public static String shp2geoJson(String shpUrl) throws IOException {
//        File shpfile = new File(shpUrl);
//        ShapefileDataStore shpDataStore = (ShapefileDataStore) new ShapefileDataStoreFactory().createDataStore(shpfile.toURI().toURL());
//        String typeName = shpDataStore.getTypeNames()[0];
//        String name[]=shpfile.getName().split("\\.");
//        String name1 = shpfile.getName();
//        int i = name1.lastIndexOf(".");
//        String substring = name1.substring(0, i);
//        if (name.length > 0) {
//            String codePage = getCodePage(shpfile.getParent()+"/"+substring+".dbf");
////           System.out.println("codePage:"+codePage);
//            assert codePage != null;
//            shpDataStore.setCharset(Charset.forName(codePage));
//        }
//        System.out.println(shpDataStore);
//        ContentFeatureSource featureSource = shpDataStore.getFeatureSource(typeName);
//        System.out.println(featureSource.getName());
//        featureSource.getFeatures();
//        return getPolygonGeoJson(featureSource.getFeatures());
//    }
//    /**
//     * shp转换为Geojson
//     *
//     * @param
//     * @return
//     */
//    public static String getPolygonGeoJson(FeatureCollection fc) {
//        FeatureJSON fjson = new FeatureJSON();
//        StringBuffer sb = new StringBuffer();
//        try {
//            sb.append("{\"type\": \"FeatureCollection\",\"features\": ");
//            FeatureIterator itertor = fc.features();
//            List<String> list = new ArrayList<String>();
//
//            while (itertor.hasNext()) {
//                SimpleFeature feature = (SimpleFeature) itertor.next();
//                StringWriter writer = new StringWriter();
//                fjson.writeFeature(feature, writer);
//                list.add(writer.toString());
//                writer.close();
//            }
//            itertor.close();
//            sb.append(list.toString());
//            sb.append("}");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return sb.toString();
//    }
//    public static String getCodePage(String path) {
//        try {
//            InputStream dbf = new FileInputStream(path);
//            byte[] bytes = new byte[30];
//            dbf.read(bytes);
//            byte b = bytes[29];
//            System.out.println(b);
//            String hexString = Integer.toHexString(Byte.toUnsignedInt(b));
//            System.out.println(hexString);
//            if ("0".equals(hexString) || hexString == "0") {
//                dbf.close();
//                return "UTF-8";
//            }
//            if (hexString.length() == 1) {
//                hexString = "0" + hexString;
//            }
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//}
