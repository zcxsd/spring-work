package com.zcx.springimagesdk;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.InfoOptions;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.ogr.ogr;
import org.gdal.osr.SpatialReference;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static com.zcx.utils.CscUtils.splitRaster;

@SpringBootTest
class SpringImagesdkApplicationTests {
    static {
        gdal.AllRegister();
    }
    @Test
    void contextLoads() {
        System.out.println("GDAL version: " + gdal.VersionInfo("RELEASE_NAME"));
    }
  @Test
    public void m1(){
          String inputPath = "G:\\XQ\\jinzita\\裁切\\polygon11.tif";
          String outputPath = "F:\\formt\\";
          splitRaster(inputPath, outputPath, 512, 512);
  }

}
