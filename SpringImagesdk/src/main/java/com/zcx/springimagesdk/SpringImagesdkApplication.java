package com.zcx.springimagesdk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringImagesdkApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringImagesdkApplication.class, args);
    }

}
