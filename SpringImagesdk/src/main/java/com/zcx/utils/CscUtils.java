package com.zcx.utils;


import org.gdal.gdal.*;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.osr.SpatialReference;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @Author:zcx
 * @DATE:2023/5/10 11:40
 * @Description:坐标系转换
 * @Version 1.0
 */
public class CscUtils {
    static {
        gdal.AllRegister();
    }

    /**
     *@Author:zcx
     *@DATE: 2023/5/10 11:36
     *@Description:转地理坐标系
     */
    /**
     * 构建warp操作的参数列表
     *
     * @param srcSrs        原坐标系
     * @param dstSrs        目标坐标系
     * @param pixelSizeX    X轴方向像素大小
     * @param pixelSizeY    Y轴方向像素大小
     * @param resampleMethod 重采样方法
     * @param outputFormat  输出影像格式
     * @param nullValue     输出影像中的无值像元
     * @return WarpOptions  包含warp操作的参数列表
     */
    public static WarpOptions buildWarpOptions(String srcSrs, String dstSrs,
                                               double pixelSizeX, double pixelSizeY,
                                               String resampleMethod, String outputFormat,
                                               double nullValue) {
        Vector warpOptions = new Vector();    // 创建向量以存储warp操作的参数列表
        warpOptions.add("-s_srs");            // 原始SRS
        warpOptions.add(srcSrs);
        warpOptions.add("-t_srs");            // 目标SRS
        warpOptions.add(dstSrs);
        warpOptions.add("-r");                // 重采样方法
        warpOptions.add(resampleMethod);
        warpOptions.add("-wm");              // 内存缓存的最大大小
        warpOptions.add("500");
        warpOptions.add("-of");              // 指定输出文件格式
        warpOptions.add(outputFormat);
//      warpOptions.add("-tr");              // 指定X和Y方向上像素的大小，如果没有就默认输入影像数据的像素大小
//      warpOptions.add(String.valueOf(pixelSizeX));
//      warpOptions.add(String.valueOf(pixelSizeY));
        /**
         * 可以使用多种方法对输出影像进行压缩，
         * 其中包括DEFLATE、LZW、PACKBITS、JPEG、LZMA等方法。
         * 默认情况下，GDAL使用打开输入影像所用的压缩方法来创建输出影像。
         * 如果输入影像未压缩，则输出文件也不会被压缩。
         * 如果输入影像已经被压缩，则GDAL会使用与输入影像相同的压缩方法来创建输出影像。
         */
        warpOptions.add("-co");               // 设置输出文件的压缩选项
        warpOptions.add("COMPRESS=DEFLATE");  // 使用DEFLATE压缩方法
        /**
         * 目前最优5线程，500M内存缓存大小，2G文件1分钟内
         */
        warpOptions.add("-wo");          //开启多线程
        warpOptions.add("NUM_THREADS=5");   //设置gdal开启线程数5
        warpOptions.add("-wm");              // 内存缓存的最大大小
        warpOptions.add("500");
        warpOptions.add("-dstnodata");       // 指定输入影像中要变成空值像素的值
        warpOptions.add(String.valueOf(nullValue));
        return new WarpOptions(warpOptions); // 使用warp操作参数创建WarpOptions对象
    }

    /**
     * 将源影像文件重投影和重采样为目标影像文件，并返回目标影像文件原点坐标
     */
    public static double[] warpRaster(String srcPath, String dstPath, String srcSrs, String dstSrs,
                                      double pixelSizeX, double pixelSizeY, String resampleMethod,
                                      String outputFormat, double nullValue) {

        Dataset srcDs = gdal.Open(srcPath, gdalconstConstants.GA_ReadOnly);

        if (srcDs == null) {
            System.err.println("GDALOpen failed - " + gdal.GetLastErrorNo());
            System.err.println(gdal.GetLastErrorMsg());
            return null;
        }

        SpatialReference srcSpatialRef = new SpatialReference(srcDs.GetProjection()); // 获取源空间参考系
        SpatialReference dstSpatialRef = new SpatialReference(dstSrs);   // 获取目标空间参考系
        if (!srcSrs.contains("EPSG")) {// 如果目标SRS包含EPSG，则使用EPSG编码 否则，使用WKT或proj4格式
            srcSpatialRef.ImportFromWkt(srcSrs);
        }
        if (!dstSrs.contains("EPSG")) {// 如果目标SRS包含EPSG，则使用EPSG编码 否则，使用WKT或proj4格式
            dstSpatialRef.ImportFromWkt(dstSrs);
        }
        double[] geoTransform = new double[6]; // 创建一个获取源数据集地理信息的数组，包括原点坐标和像素大小
        srcDs.GetGeoTransform(geoTransform);   // 获取源数据集的地理信息

        WarpOptions options = buildWarpOptions(srcSrs, dstSrs, pixelSizeX, pixelSizeY,
                resampleMethod, outputFormat, nullValue);// 创建WarpOptions对象
        ProgressReporter progressReporter=new ProgressReporter();
        Dataset dstDs = gdal.Warp(dstPath, new Dataset[]{srcDs}, options,progressReporter);

        double[] warpTransform = new double[6]; // 创建一个数组来获取地理信息，以确定目标数据集的原点
        dstDs.GetGeoTransform(warpTransform);// 获取目标数据集的地理信息

        srcDs.delete(); // 删除源数据集
        dstDs.delete(); // 删除目标数据集

        return warpTransform;// 返回目标数据集的地理信息(即原点坐标和像素大小)数组
    }




   /**
     *@Author:zcx
     *@DATE: 2023/5/10 11:36
     *@Description:转投影坐标系
   */
    /**
     * 根据源和目标空间参考、像素大小、重采样方法、输出格式和无效值创建Warp选项
     * @param srcSpatialRef 源空间参考
     * @param dstSpatialRef 目标空间参考
     * @param pixelSizeX 像素X方向大小
     * @param pixelSizeY 像素Y方向大小
     * @param resampleMethod 重采样方法
     * @param outputFormat 输出格式
     * @param nullValue 无效值
     * @return WarpOptions对象，包含Warp过程的选项
     */
    public static WarpOptions buildWarpOptions(SpatialReference srcSpatialRef, SpatialReference dstSpatialRef,
                                               double pixelSizeX, double pixelSizeY,
                                               String resampleMethod, String outputFormat,
                                               double nullValue) {
        // 创建一个字符串向量，用于存储Warp过程的选项
        Vector<String> warpOptions = new Vector<>();
        // 设置源空间参考
        warpOptions.add("-s_srs");
        warpOptions.add(srcSpatialRef.ExportToWkt());
        // 设置目标空间参考
        warpOptions.add("-t_srs");
        warpOptions.add(dstSpatialRef.ExportToWkt());
        // 设置重采样方法
        warpOptions.add("-r");
        warpOptions.add(resampleMethod);
        // 设置内存使用限制为500MB
        warpOptions.add("-wm");
        warpOptions.add("500");
        //输出格式
        warpOptions.add("-of");
        warpOptions.add(outputFormat);
//        // 设置像素大小
//        warpOptions.add("-tr");
//        warpOptions.add(String.valueOf(pixelSizeX));
//        warpOptions.add(String.valueOf(pixelSizeY));
        /**
         * 可以使用多种方法对输出影像进行压缩，
         * 其中包括DEFLATE、LZW、PACKBITS、JPEG、LZMA等方法。
         * 默认情况下，GDAL使用打开输入影像所用的压缩方法来创建输出影像。
         * 如果输入影像未压缩，则输出文件也不会被压缩。
         * 如果输入影像已经被压缩，则GDAL会使用与输入影像相同的压缩方法来创建输出影像。
         */
        warpOptions.add("-co");               // 设置输出文件的压缩选项
        warpOptions.add("COMPRESS=DEFLATE");  // 使用DEFLATE压缩方法
        /**
         * 目前最优5线程，500M内存缓存大小，2G文件1分钟内
         */
        warpOptions.add("-wo");          //开启多线程
        warpOptions.add("NUM_THREADS=5");   //设置gdal开启线程数5
        warpOptions.add("-wm");              // 内存缓存的最大大小
        warpOptions.add("500");
        // 设置无效值
        warpOptions.add("-dstnodata");
        warpOptions.add(String.valueOf(nullValue));
        // 返回包含Warp过程选项的WarpOptions对象
        return new WarpOptions(warpOptions);
    }

    /**
     * 根据源文件路径、目标文件路径、源空间参考、目标空间参考、像素大小、重采样方法、输出格式和无效值进行栅格Warp
     * @param srcPath 源文件路径
     * @param dstPath 目标文件路径
     * @param srcSpatialRef 源空间参考
     * @param dstSpatialRef 目标空间参考
     * @param pixelSizeX 像素X方向大小
     * @param pixelSizeY 像素Y方向大小
     * @param resampleMethod 重采样方法
     * @param outputFormat 输出格式
     * @param nullValue 无效值
     * @return double[]类型数组，包含栅格Warp后的仿射变换系数
     */
    public static double[] warpRaster(String srcPath, String dstPath, SpatialReference srcSpatialRef, SpatialReference dstSpatialRef,
                                      double pixelSizeX, double pixelSizeY, String resampleMethod,
                                      String outputFormat, double nullValue) {

        Dataset srcDs = gdal.Open(srcPath, gdalconstConstants.GA_ReadOnly);

        if (srcDs == null) {
            System.err.println("GDALOpen failed - " + gdal.GetLastErrorNo());
            System.err.println(gdal.GetLastErrorMsg());
            return null;
        }
        // 根据输入参数构建Warp选项
        WarpOptions options = buildWarpOptions(srcSpatialRef, dstSpatialRef, pixelSizeX, pixelSizeY,
                resampleMethod, outputFormat, nullValue);
        ProgressReporter progressReporter=new ProgressReporter();
        Dataset dstDs = gdal.Warp(dstPath, new Dataset[]{srcDs}, options, progressReporter);
        // 获取Warp后的仿射变换系数
        double[] warpTransform = new double[6];
        dstDs.GetGeoTransform(warpTransform);

        srcDs.delete();
        dstDs.delete();
        // 返回栅格Warp后的仿射变换系数
        return warpTransform;
    }

    public static void formDpsg(String srcPath,String dstPath, String srcSrs,String dstSrs,double  pixelSizeX,double pixelSizeY ,String resampleMethod,String outputFormat) {
        double nullValue = 0.0;
        // 创建源空间参考并导入EPSG坐标系统
        SpatialReference srcSpatialRef = new SpatialReference();
//        srcSpatialRef.ImportFromEPSG(srcSrs);
        if (srcSrs.matches("^\\d+$")) {
            // If EPSG code, import from EPSG
            srcSpatialRef.ImportFromEPSG(Integer.parseInt(srcSrs));
        } else {
            // If coordinate system string, import from WKT
            srcSpatialRef.ImportFromWkt(srcSrs);
        }
        // 创建目标空间参考并导入EPSG坐标系统
        SpatialReference dstSpatialRef = new SpatialReference();
        dstSpatialRef.ImportFromEPSG(Integer.parseInt(dstSrs));
        // 调用warpRaster方法进行栅格Warp操作
        double[] warpTransform = warpRaster(srcPath, dstPath, srcSpatialRef, dstSpatialRef, pixelSizeX, pixelSizeY, resampleMethod, outputFormat, nullValue);
        System.out.println("Warp completed successfully");
        System.out.println("Origin X: " + warpTransform[0]);
        System.out.println("Origin Y: " + warpTransform[3]);
        System.out.println("Pixel Size X: " + warpTransform[1]);
        System.out.println("Pixel Size Y: " + warpTransform[5]);
    }
    //创建金字塔
    public static void createPy(String input,String output){
//        gdal.AllRegister();  // 注册 GDAL 驱动
        // 打开输入数据集
        Dataset inputDataset = gdal.Open(input, gdalconstConstants.GA_ReadOnly);
        Vector warpOptions = new Vector();    // 创建向量以存储warp操作的参数列表
        warpOptions.add("-co");
        warpOptions.add("TILED=YES");
        warpOptions.add("-co");               // 设置输出文件的压缩选项
        warpOptions.add("COMPRESS=DEFLATE");  // 使用DEFLATE压缩方法
        warpOptions.add("-co");
        warpOptions.add("COPY_SRC_OVERVIEWS=YES");
        // 创建金字塔
        warpOptions.add("-r");
        warpOptions.add("average");
        warpOptions.add("-levels");
        warpOptions.add("4");
        warpOptions.add("-ovr");
        // 设置输出文件格式为.ovr
        TranslateOptions translateOptions = new TranslateOptions(warpOptions);
        ProgressReporter progressReporter=new ProgressReporter();
        Dataset outputDataset = gdal.Translate(output, inputDataset, translateOptions,progressReporter);
        // 释放资源
        inputDataset.delete();
        outputDataset.delete();
//        gdal.GDALDestroyDriverManager();
    }

    public static void createPy(String input){
        gdal.AllRegister();
        Dataset dataset = gdal.Open(input, gdalconstConstants.GA_ReadOnly);
        //调用构建金字塔接口
        dataset.BuildOverviews("nearest",new int[]{2,4,6,8,16});
        dataset.delete();
    }
    //生成缩略图
    public static void createThumbnail(String input, String output, int width, int height) {
        // 打开输入数据集
        Dataset inputDataset = gdal.Open(input, gdalconstConstants.GA_ReadOnly);
        // 创建向量以存储 warp 操作的参数列表
        Vector<String> warpOptions = new Vector<String>();
        warpOptions.add("-of"); // 指定输出格式为 JPEG
        warpOptions.add("PNG");
        warpOptions.add("-outsize"); // 指定输出图像的大小
        warpOptions.add(Integer.toString(width));
        warpOptions.add(Integer.toString(height));
        warpOptions.add("-co"); // 设置 JPEG 压缩质量
        warpOptions.add("PHOTOMETRIC=YCBCR"); // 使用 YCbCr 色彩空间
        warpOptions.add("JPEG_QUALITY=100");
        //warpOptions.add("JPEG_SUBSAMPLING=2,1,1"); // 使用 YUV422 子采样
        TranslateOptions translateOptions = new TranslateOptions(warpOptions);
        ProgressReporter progressReporter=new ProgressReporter();
        Dataset outputDataset = gdal.Translate(output, inputDataset, translateOptions,progressReporter);
        // 释放资源
        inputDataset.delete();
        outputDataset.delete();
        // 销毁驱动管理器
//        gdal.GDALDestroyDriverManager();
    }

    /**
     * Description:读取影像数据包围盒
     * @param filePath
     * @return：kv对象数据集
     */
    public static Map<String,Double> getImageXY(String filePath){
        double coverageMinX = 0.0; //首先初始化图像最小经度为0
        double coverageMaxX = 0.0; //然后初始化图像最大经度为0
        double coverageMinY = 0.0; //接着初始化图像最小纬度为0
        double coverageMaxY = 0.0; //最后初始化图像最大纬度为0
        Dataset dataset = gdal.Open(filePath, gdalconstConstants.GA_ReadOnly); //打开GDAL数据集
        double[] adfGeoTransform = dataset.GetGeoTransform(); //获取GDAL数据集的地理变换信息
        coverageMinX = adfGeoTransform[0]; //获取图像最小经度
        coverageMaxY = adfGeoTransform[3]; //获取图像最大纬度
        coverageMaxX = coverageMinX + adfGeoTransform[1] * dataset.getRasterXSize(); //获取图像最大经度
        coverageMinY = coverageMaxY + adfGeoTransform[5] * dataset.getRasterYSize(); //获取图像最小纬度
        System.out.println("MinX: " + coverageMinX); //输出图像最小经度
        System.out.println("MaxX: " + coverageMaxX); //输出图像最大经度
        System.out.println("MinY: " + coverageMinY); //输出图像最小纬度
        System.out.println("MaxY: " + coverageMaxY); //输出图像最大纬度
        dataset.delete();
        Map<String, Double> map=new HashMap<>();
        map.put("coverageMinX",coverageMinX);
        map.put("coverageMaxX",coverageMaxX);
        map.put("coverageMinY",coverageMinY);
        map.put("coverageMaxY",coverageMaxY);

        return map;
    }



    public static void splitRaster(String inputPath, String outputPath, int blockWidth, int blockHeight) {
        File inputFile = new File(inputPath);
        Dataset inputDataset = gdal.Open(inputPath, gdalconstConstants.GA_ReadOnly);

        int width = inputDataset.GetRasterXSize();
        int height = inputDataset.GetRasterYSize();

        int blocksX = (int) Math.ceil((double) width / blockWidth);
        int blocksY = (int) Math.ceil((double) height / blockHeight);

        for (int i = 0; i < blocksY; i++) {
            for (int j = 0; j < blocksX; j++) {
                int offsetX = j * blockWidth;
                int offsetY = i * blockHeight;
                int actualBlockWidth = Math.min(blockWidth, width - offsetX);
                int actualBlockHeight = Math.min(blockHeight, height - offsetY);

                Dataset outputDataset = gdal.GetDriverByName("GTiff").Create(
                        outputPath + File.separator + inputFile.getName() + "_" + i + "_" + j + ".tif",
                        actualBlockWidth, actualBlockHeight, inputDataset.GetRasterCount(),
                        inputDataset.GetRasterBand(1).getDataType());

                outputDataset.SetGeoTransform(new double[]{
                        inputDataset.GetGeoTransform()[0] + offsetX * inputDataset.GetGeoTransform()[1],
                        inputDataset.GetGeoTransform()[1],
                        inputDataset.GetGeoTransform()[2],
                        inputDataset.GetGeoTransform()[3] + offsetY * inputDataset.GetGeoTransform()[5],
                        inputDataset.GetGeoTransform()[4],
                        inputDataset.GetGeoTransform()[5]
                });

                outputDataset.SetProjection(inputDataset.GetProjection());

                for (int band = 1; band <= inputDataset.GetRasterCount(); band++) {
                    byte[] data = new byte[actualBlockWidth * actualBlockHeight];
                    inputDataset.GetRasterBand(band).ReadRaster(offsetX, offsetY, actualBlockWidth, actualBlockHeight, data);
                    outputDataset.GetRasterBand(band).WriteRaster(0, 0, actualBlockWidth, actualBlockHeight, data);
                }

                outputDataset.FlushCache();
                outputDataset.delete();
            }
        }

        inputDataset.delete();
    }

    public static void splitImage3s(String inputFilePath, String outputFolder, int bitDepth) {
        // 打开输入影像
        Dataset inputDs = gdal.Open(inputFilePath, gdalconstConstants.GA_ReadOnly);
        // 获取输入影像的宽度和高度
        int width = inputDs.getRasterXSize();
        int height = inputDs.getRasterYSize();
        // 计算切割的数量
        int numColumns = (width + 511) / 512;
        int numRows = (height + 511) / 512;
        int blockCount = 0;
        // 设置输出选项
        Vector<String> translateOptionVec = new Vector<>();
        translateOptionVec.add("-of");
        translateOptionVec.add("GTiff");
        // 复制输入影像的坐标系信息
        translateOptionVec.add("-a_srs");
        translateOptionVec.add(inputDs.GetProjectionRef());
        // 设置插值算法为Bilinear插值算法
//        translateOptionVec.add("-r");
//        translateOptionVec.add("bilinear");
        translateOptionVec.add("-co");               // 设置输出文件的压缩选项
        translateOptionVec.add("COMPRESS=DEFLATE");  // 使用DEFLATE压缩方法
        // 设置输出影像的色深度
//        translateOptionVec.add("-ot");
//        translateOptionVec.add(String.format("Int%d", bitDepth));
        Map<String,TranslateOptions> map=new HashMap<>();
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numColumns; j++) {
                // 计算切割区域
                int xMin = j * 512;
                int yMin = i * 512;
                int xMax = Math.min(xMin + 512, width);
                int yMax = Math.min(yMin + 512, height);
                int xSize = xMax - xMin;
                int ySize = yMax - yMin;
                // 创建输出文件名
                String outputFilePath = String.format("%s/tile_%d.tif", outputFolder, blockCount++);
                // 设置输出区域和选项
                translateOptionVec.add("-srcwin");
                translateOptionVec.add(Integer.toString(xMin));
                translateOptionVec.add(Integer.toString(yMin));
                translateOptionVec.add(Integer.toString(xSize));
                translateOptionVec.add(Integer.toString(ySize));
                TranslateOptions options = new TranslateOptions(translateOptionVec);
                // 切割并保存影像
                ProgressReporter progressReporter=new ProgressReporter();
                map.put(outputFilePath,options);
//                gdal.Translate(outputFilePath, inputDs, options,progressReporter);
            }
        }
        // 记录开始时间
        long startTime = System.currentTimeMillis();


        ForkJoinPool forkJoinPool=new ForkJoinPool();
        Mycuttask mycuttask=new Mycuttask(inputDs,map);
        ForkJoinTask<Integer> forkJoinTask=forkJoinPool.submit(mycuttask);
        try {
            System.out.println(forkJoinTask.get());
            // 记录结束时间
            long endTime = System.currentTimeMillis();
            // 输出执行时间
            System.out.println("执行时间：" + (endTime - startTime) + "ms");
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        // 关闭输入影像
        inputDs.delete();
    }
    static class Mycuttask extends RecursiveTask<Integer> {
        private Dataset dataset;

        private Map<String,TranslateOptions> map;
        public Mycuttask(Dataset dataset ,Map map){
            this.dataset=dataset;
            this.map=map;
        }
        @Override
        protected Integer compute() {
            if (map.size()<=30){

                for (Map.Entry<String,TranslateOptions> m:map.entrySet()){
                    String outputFilePath= m.getKey();
                    TranslateOptions TranslateOptions = m.getValue();
                    gdal.Translate(outputFilePath, dataset, TranslateOptions);
                }
            }else {
                // 获取map的entrySet
                Set<Map.Entry<String, TranslateOptions>> entrySet = map.entrySet();
                List<Map.Entry<String, TranslateOptions>> entryList = new ArrayList<>(entrySet);
                int size = entryList.size();
                int partSize = (int) Math.ceil((double) size / 3);
                List<List<Map.Entry<String, TranslateOptions>>> parts = IntStream.range(0, 3)
                        .mapToObj(i -> entryList.subList(i * partSize, Math.min((i + 1) * partSize, size)))
                        .collect(Collectors.toList());
                int m=0;
                // 遍历每一份进行操作
                for (List<Map.Entry<String, TranslateOptions>> part : parts) {
                    // 对于每一份，将其转换为一个新的Map
                    Map<String, TranslateOptions> subMap = part.stream()
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                    // 进行操作，例如切割影像等
                    Mycuttask mycuttask1=new Mycuttask(dataset,subMap);
                    mycuttask1.fork();
                    m+=mycuttask1.join();
                }

                return m;
            }

            return 1;
        }
    }
    public static void Mosaic2() {
        gdal.AllRegister();
        String[] inputFiles = new String[]{"G:\\XQ\\peizhun\\1-3\\JL1GF03B01_PMS_20210328101035_200045489_101_0003_001_L1_MSS.tif", "G:\\XQ\\peizhun\\1-3\\JL1GF03B01_PMS_20210328101035_200045489_101_0004_001_L1_MSS.tif"};
        String outputFile = "F:\\formt\\s\\4.tif";
        Dataset[] inputDatasets = new Dataset[inputFiles.length];
        WarpOptions options=buildWarpOptionsMosaic2();
        for (int i = 0; i < inputFiles.length; i++) {
            inputDatasets[i] = gdal.Open(inputFiles[i], gdalconstConstants.GA_ReadOnly);
        }
        Dataset outputDataset = gdal.GetDriverByName("GTiff").Create(outputFile, inputDatasets[0].GetRasterXSize(), inputDatasets[0].GetRasterYSize(), inputDatasets[0].GetRasterCount());
        gdal.Warp(outputDataset, inputDatasets, options);
        for (Dataset dataset : inputDatasets) {
            dataset.delete();
        }
        outputDataset.delete();
    }
    public static WarpOptions buildWarpOptionsMosaic2() {
        Vector<String> translateOptionVec = new Vector<>();
        translateOptionVec.add("-srcnodata");
        translateOptionVec.add("0");
        translateOptionVec.add("-dstnodata");
        translateOptionVec.add("0");
        translateOptionVec.add("-t_srs");
        translateOptionVec.add("EPSG:4326");
        translateOptionVec.add("-of");
        translateOptionVec.add("GTiff");
        translateOptionVec.add("DST_METHOD=NO_GEOTRANSFORM"); // 在这里设置选项
        return new WarpOptions(translateOptionVec); // 使用warp操作参数创建WarpOptions对象
    }



    //转投影坐标系示例
    public static void main(String[] args) {
        Mosaic2();
//        createPy("D:\\data\\data\\file\\空间数据\\栅格数据\\polygon11_范围2.1_0_0.tif","D:\\data\\data\\file\\空间数据\\栅格数据\\polygon11_范围2.1_0_0.tif");
//        gdal.AllRegister();
//        Dataset dataset = gdal.Open("D:\\data\\data\\file\\空间数据\\栅格数据\\polygon11_范围2.1_0_0.tif", gdalconstConstants.GA_ReadOnly);
//        //调用构建金字塔接口
//        dataset.BuildOverviews("nearest",new int[]{2,4,6,8,16});


      //  createThumbnail("E:\\testfile\\testtif\\YSDEM_0409.tif","E:\\testfile\\testtif\\YSDEM_0409.jpg",100,100);
//        String srcPath = "F:\\formt\\32615.tif";
//        String dstPath = "F:\\formt\\4326.tif";
//        // 打开影像数据
//        Dataset dataset = gdal.Open(srcPath, gdalconst.GA_ReadOnly);
//        if (dataset == null) {
//            System.err.println("影像数据打开失败");
//
//        }
//        //   获取影像数据坐标系信息
//        String projection = dataset.GetProjection();
//        //srcSrs可以传epsg也可以传wkt
//        String srcSrs = "32615";
//        //srcSrs=projection;
//        String dstSrs = "4326";
//        double pixelSizeX = 30.0;
//        double pixelSizeY = 30.0;
//        // 重采样方法
//        String resampleMethod = "near";
//        // 输出格式
//        String outputFormat = "GTiff";//tif->GTiff,img->HFA,其他参考https://www.cnblogs.com/zcxxcvbn/p/16899866.html
//        formDpsg(srcPath,dstPath,srcSrs,dstSrs,pixelSizeX,pixelSizeY,resampleMethod,outputFormat);

    }
//    public static void main(String[] args) {
//        //转地理坐标系示例
//        String srcPath="F:\\formt\\4610.tif";
//        // 打开影像数据
//        Dataset dataset = gdal.Open(srcPath, gdalconst.GA_ReadOnly);
//        if (dataset == null) {
//            System.err.println("影像数据打开失败");
//
//        }
//        //   获取影像数据坐标系信息
//        String projection = dataset.GetProjection();
//
//        double[] warpTransform = warpRaster(
//                srcPath,
//                "F:\\formt\\14326.tif",
//                projection,
//                "EPSG:4326",
//                0.0000101,
//                0.0000101,
//                "bilinear",
//                "GTiff",
//                0.0);
//        System.out.println("warp原点坐标   = " + warpTransform[0] + "," + warpTransform[3]);
//        System.out.println("warp像素坐标差 = " + warpTransform[1] + "," + warpTransform[5]);
//    }


}
