package com.zcx.utils;


import org.gdal.gdal.Dataset;
import org.gdal.gdal.WarpOptions;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.ogr.ogr;

import java.io.File;
import java.util.Vector;

/**
 * @Author:zcx
 * @DATE:2023/5/6 13:20
 * @Description:shp裁切tif
 * @Version 1.0
 */
public class CutimageUtils {
    static {
        gdal.AllRegister();
        ogr.RegisterAll();
    }
    public static void main(String[] args) {
        String imgURL = "G:\\XQ\\jinzita\\裁切\\polygon11.tif";
        String outURL = "F:\\formt\\caiqiemianp.tif";
        String shpURL = "G:\\XQ\\jinzita\\裁切\\caiqiemian.shp";
        Thread t=new Thread(()->{
            ProgressReporter progressReporter=new ProgressReporter();
            progressReporter.mytask(234232);
            // 记录开始时间
            long startTime = System.currentTimeMillis();
            cutImage(imgURL, outURL, shpURL,"GTiff",progressReporter);
            // 记录结束时间
            long endTime = System.currentTimeMillis();
            // 输出执行时间
            System.out.println("执行时间：" + (endTime - startTime) + "ms");
        });
        t.start();

    }

    public static void cutImage(String imgURL, String outURL, String shpURL,String format,ProgressReporter progressReporter) {
        File file = new File(shpURL);
        String shpLayerName = file.getName().substring(0, file.getName().lastIndexOf("."));
        try {
            Dataset srcDataset = gdal.Open(imgURL, gdalconstConstants.GA_ReadOnly);
            if (srcDataset == null) {
                throw new RuntimeException("GDALOpen failed - " + gdal.GetLastErrorNo());
            }
            // 创建warpOptions对象，用于裁剪
            WarpOptions warpOptions = createWarpOptions(shpURL, shpLayerName, format);
            // 对图像进行裁剪
            Dataset outputDataset = gdal.Warp(outURL, new Dataset[]{srcDataset}, warpOptions, progressReporter);
            // 获取输出图像的地理转换信息
            double[] warpTransform = outputDataset.GetGeoTransform();
            System.out.println("warp原点坐标   =  " + warpTransform[0] + "," + warpTransform[3]);
            System.out.println("warp像素坐标差 = " + warpTransform[1] + "," + warpTransform[5]);
            // 删除输出图像和原始图像
            outputDataset.delete();
            srcDataset.delete();
            warpOptions.delete();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static WarpOptions createWarpOptions(String shpURL, String shpLayerName,String format) {
        // 构建warp参数
        Vector<String> options = new Vector<>();
        options.add("-of");
        options.add(format); //GTiff->tif,img->HAF
        options.add("-cutline");//按照矢量图像线裁剪
        options.add(shpURL);
        options.add("-cl");
        options.add(shpLayerName);
        options.add("-crop_to_cutline"); //裁剪后图层范围使用矢量图层大小
//        options.add("-tr");              // 指定X和Y方向上像素的大小，如果没有就默认输入影像数据的像素大小
//        options.add(String.valueOf(x));
//        options.add(String.valueOf(y));
        /**
         * 目前最优5线程，500M内存缓存大小，2G文件1分钟内
         */
//        options.add("-wo");          //开启多线程
//        options.add("NUM_THREADS=1");   //设置gdal开启线程数5
        options.add("-wm");              // 内存缓存的最大大小
        options.add("500");
        options.add("-dstnodata");
        options.add("0.0"); //nulldata的值
        // 返回WarpOptions对象
        return new WarpOptions(options);
    }




}
