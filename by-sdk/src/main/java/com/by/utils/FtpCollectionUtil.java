//package com.by.utils;
//
///**
// * @Author:
// * @DATE:2023/2/10 9:42
// * @Description:
// * @Version 1.0
// */
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import org.apache.commons.net.ftp.FTP;
//import org.apache.commons.net.ftp.FTPClient;
//import org.apache.commons.net.ftp.FTPReply;
///**
// * 采用的是apache commons-net架包中的ftp工具类实现的
// * @author zcx
// */
//public class FtpCollectionUtil {
//    private final String username;
//    private final String password;
//    private final String ftpHostName;
//    private int port = 21;
//    private final FTPClient ftpClient = new FTPClient();
//    private FileOutputStream fos = null;
//    public FtpCollectionUtil(String username, String password, String ftpHostName, int port) {
//        super();
//        this.username = username;
//        this.password = password;
//        this.ftpHostName = ftpHostName;
//        this.port = port;
//    }
//
//    /**
//     * 建立连接
//     */
//    private void connect() {
//        try {
//            ftpClient.connect(ftpHostName, port);
//            int reply = ftpClient.getReplyCode();
//            if (!FTPReply.isPositiveCompletion(reply)) {
//                ftpClient.disconnect();
//            }
//            ftpClient.login(username, password);
//            ftpClient.setBufferSize(256);
//            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
//            ftpClient.setControlEncoding("utf8");
//        } catch (IOException ignored) {
//           ignored.printStackTrace();
//        }
//    }
//    /**
//     * 关闭输入输出流
//     *
//     * @param fos
//     */
//    private void close(FileOutputStream fos) {
//        try {
//            if (fos != null) {
//                fos.close();
//            }
//            ftpClient.logout();
//            ftpClient.disconnect();
//        } catch (IOException ignored) {
//        }
//    }
//    /**
//     * 下载文件
//     *
//     * @param ftpFileName
//     * @param localDir
//     */
//    public void down(String ftpFileName, String localDir) {
//        connect();
//        downFileOrDir(ftpFileName, localDir);
//        close(fos);
//    }
//    private void downFileOrDir(String ftpFileName, String localDir) {
//        try {
//            File file = new File(ftpFileName);
//            File temp = new File(localDir);
//            if (!temp.exists()) {
//                temp.mkdirs();
//            }
//// 判断是否是目录
//            if (isDir(ftpFileName)) {
//                String[] names = ftpClient.listNames();
//                for (String name : names) {
//                    System.out.println(name + "^^^^^^^^^^^^^^");
//                    if (!isDir(name)) {
////                        downFileOrDir(ftpFileName + '/' + name, localDir + File.separator + name);
////                        ftpClient.changeToParentDirectory()
//                        File localfile = new File(localDir + File.separator + name);
//                        if (!localfile.exists()) {
//                            fos = new FileOutputStream(localfile);
//                            ftpClient.retrieveFile(name, fos);
//                        } else {
//                            file.delete();
//                            fos = new FileOutputStream(localfile);
//                            ftpClient.retrieveFile(ftpFileName, fos);
//                        }
//                    }
//                }
//            }
////            else {
////                File localfile = new File(localDir + File.separator + file.getName());
////                if (!localfile.exists()) {
////                    fos = new FileOutputStream(localfile);
////                    ftpClient.retrieveFile(ftpFileName, fos);
////                } else {
////                    file.delete();
////                    fos = new FileOutputStream(localfile);
////                    ftpClient.retrieveFile(ftpFileName, fos);
////                }
////                ftpClient.changeToParentDirectory();
////            }
//        } catch (IOException ignored) {
//            ignored.printStackTrace();
//        }
//    }
//    // 判断是否是目录
//    public boolean isDir(String fileName) {
//        try {
//// 切换目录，若当前是目录则返回true,否则返回true。
//            boolean falg = ftpClient.changeWorkingDirectory(fileName);
//            return falg;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//    public static void main(String[] args) {
///**
// * apache common-net实现的
// */
//        FtpCollectionUtil ftpUtil = new FtpCollectionUtil("ftpuser", "123", "192.168.0.182", 21);
//// /home/webfocus1/apache-tomcat-6.0.37/webapps/NEZA_ROOT/要下载的文件夹。
//        ftpUtil.down("/", "G:\\jar");
//    }
//}
