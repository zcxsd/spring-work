package com.by.utils;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author:
 * @DATE:2023/2/15 13:11
 * @Description:
 * @Version 1.0
 */
public class ArrayUtils {
    public static void main(String[] args) {
        String[] array=new String[]{"1","2","3","4","5","6","7","8","9","0"};
        System.out.println(Arrays.toString(Arrays.stream(array).toArray()));
        String[] arrays=new String[10];
        Arrays.sort(array);
        System.out.println(Arrays.toString(Arrays.stream(array).toArray()));

        System.out.println(4.0-3.6);
        double mn=4.0-3.6;
        float d=4.0f;
        float m=3.6f;
        System.out.println(mn);
        System.out.println(d-m);
        BigDecimal k=new BigDecimal(4.0);
        BigDecimal l=new BigDecimal(3.6);
        System.out.println(k.subtract(l).floatValue());
        List<String> list=new ArrayList<>(Arrays.asList(array));
        Stream<String> stream=list.stream();
        Stream<String> parallelStream=list.parallelStream();
        System.out.println(list);
        List<String> sl=new ArrayList<>();
        String r=list.stream().map((s) -> "'" + s + "'").collect(Collectors.joining(","));
        String kwa="NAD|BWD|FWD";
        String [] strings=kwa.split("\\|");
        for (String lk:strings){
            System.out.println(lk);
        }
        System.out.println(Arrays.toString(strings));
        System.out.println(list.stream().filter(s -> !s.contains("1")).count());
        System.out.println(list.stream().map((s) -> "'" + s + "'").collect(Collectors.joining(",")));
        File f=new File("E:\\workspace\\hb-ImageManagement2.0-java\\src\\main\\webapp\\upload\\shp\\caiqiemian");
        f.deleteOnExit();
        f.delete();

    }
}
