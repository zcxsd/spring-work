package com.by.utils;

import java.util.Map;

public class MapUtils {

    private static final String PAGENUM="pageNum";
    private static final String PAGESIZE="pageSize";

    /**
     * 获取map中指定key的值 转换为int类型
     *
     * @param map
     * @param key
     */
    public static int getIntValue(Map map, String key) {
        return getDefaultIntValue(map, key, 0);
    }

    /**
     * 获取分页参数 pageNum 如果不存在 返回1
     *
     * @param map
     * @return
     */
    public static int getPageNumValue(Map map) {
        return getDefaultIntValue(map, PAGENUM, 1);
    }

    /**
     * 获取分页参数 pageSize  如果不存在 返回10
     * @param map
     * @return
     */
    public static int getPageSizeValue(Map map){
        return getDefaultIntValue(map, PAGESIZE, 10);
    }

    /**
     * 取值  如果不存在返回默认值
     *
     * @param map
     * @param key
     * @param defaultValue
     * @return
     */
    private static int getDefaultIntValue(Map map, String key, int defaultValue) {
        if (null == map || null == key || !map.containsKey(key))
            return defaultValue;
        Object tmp = map.get(key);
        return Integer.parseInt(tmp.toString());
    }
}
