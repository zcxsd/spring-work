package com.by.utils;
import java.io.*;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
public class Ftputils {
    private final String username;
    private final String password;
    private final String ftpHostName;
    private int port = 21;
    private final FTPClient ftpClient = new FTPClient();
    private FileOutputStream fos = null;
    public Ftputils(String username, String password, String ftpHostName, int port) {
        super();
        this.username = username;
        this.password = password;
        this.ftpHostName = ftpHostName;
        this.port = port;
    }
    /**
     * 建立连接
     */
    private void connect() {
        try {
            ftpClient.setControlEncoding("GBK");
            ftpClient.connect(ftpHostName, port);
            int reply = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftpClient.disconnect();
            }
            ftpClient.login(username, password);
            ftpClient.setBufferSize(256);
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

        } catch (IOException ignored) {
           ignored.printStackTrace();
        }
    }
    /**
     * 关闭输入输出流
     *
     * @param fos
     */
    private void close(FileOutputStream fos) {
        try {
            if (fos != null) {
                fos.close();
            }
            ftpClient.logout();
            ftpClient.disconnect();
        } catch (IOException ignored) {
        }
    }
    /**
     * 下载文件
     *
     * @param ftpFileName
     * @param localDir
     */
    public void down(String ftpFileName, String localDir) {
        connect();
        downFileOrDir(ftpFileName, localDir);
        close(fos);
    }
    private void downFileOrDir(String ftpFileName, String localDir) {
        try {
            File temp = new File(localDir);
            if (!temp.exists()) {
                temp.mkdirs();
            }
// 判断是否是目录
            if (isDir(ftpFileName)) {
//                ftpClient.setControlEncoding("GBK");
                FTPFile[] f= ftpClient.listFiles();
                for (FTPFile  ftpFile : f) {
                    if (!isDir(ftpFile.getName())) {
                        byte[] bytes=ftpFile.getName().getBytes("GBK");
//                        System.out.println(ftpFile.getName() + "正在下载");
                        File localfile = new File(localDir + File.separator + new String(bytes,"GBK"));
                        if (!localfile.exists()) {
                          fos = new FileOutputStream(localfile);
                          ftpClient.retrieveFile(ftpFile.getName(), fos);
                        } else {
                            String is=localDir+"\\"+ftpFile.getName();
                            File is_true=new File(is);
                            is_true.delete();
                            fos = new FileOutputStream(localfile);
                            ftpClient.retrieveFile(ftpFileName, fos);
                        }
                    }
                }
            }
        } catch (IOException ignored) {
            ignored.printStackTrace();
        }
    }
    private void upFileOrDir(String saveFileName,InputStream f) throws IOException {
        connect();
        ftpClient.storeFile(saveFileName,f);
        f.close();
    }
    // 判断是否是目录
    public boolean isDir(String fileName) {
        try {
// 切换目录，若当前是目录则返回true,否则返回true。
            boolean falg = ftpClient.changeWorkingDirectory(fileName);
            return falg;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public static void main(String[] args) throws IOException {
/**
 * apache common-net实现的
 */
        Ftputils ftpUtil = new Ftputils("ftpuser", "123", "192.168.0.182", 21);
       //下载规则：以绝对路径下载 /表示ftp根目录 /abc/表示根目录下的abc目录
        ftpUtil.down("/newData\\yx_fuori\\GF2_PMS1_E113.7_N29.8_20211206_L1A0006113622-MSS1.tiff", "G:\\jar\\jar");
        String str="G:\\jar\\gdal.jar";
        InputStream f=new FileInputStream(str);
       // ftpUtil.upFileOrDir("/newData\\gdal.jar",f);
    }
}

