package com.by;

import org.apache.commons.net.ftp.FTPClient;
import org.junit.Test;

import java.io.*;
import java.net.SocketException;

/**
 * @Author:
 * @DATE:2023/2/10 9:07
 * @Description:
 * @Version 1.0
 */

public class Ftp {
  @Test
  public void m1(){
      String downloadFileName = "GF2_PMS1_E113.7_N29.8_20211206_L1A0006113622-MSS1.tiff";
      FTPClient ftpClient = new FTPClient();
      try (FileOutputStream out = new FileOutputStream("G:\\jar\\" + downloadFileName)) {
          ftpClient.connect("192.168.0.182", 21);// 连接
          ftpClient.login("ftpuser", "123t");// 登录
          ftpClient.changeWorkingDirectory("1");// 切换目录

          // 下载文件(获取FTP服务器指定目录(1)的文件)
          // 参数1：服务器指定文件
          // 参数2：本地输出流(负责下载后写入)
          ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
          Boolean isRetrieve = ftpClient.retrieveFile(downloadFileName, out);
          System.out.println("下载成功" + isRetrieve);
      } catch (IOException e) {
          e.printStackTrace();
      } finally {
          try {
              ftpClient.disconnect();
          } catch (IOException e) {
              e.printStackTrace();
          }
      }


  }

}
