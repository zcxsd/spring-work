package com.zcx.springsecurity2.mapper;

import com.zcx.springsecurity2.entity.Role;
import com.zcx.springsecurity2.entity.User;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

/**
 * @Author:
 * @DATE:2023/7/3 10:05
 * @Description:
 * @Version 1.0
 */
@Mapper
public interface UserMapper {

    //查询用户信息
    User loadUserByUsername(String username) ;

    //根据用户id查询用户角色信息
    List<Role> getRolesByUserId(Integer id);
}
