package com.zcx.springsecurity2.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author:
 * @DATE:2023/6/19 11:18
 * @Description:
 * @Version 1.0
 */
@RestController
@Api
public class IndexController {
    @RequestMapping("/index")
    public String index(){
        System.out.println("index");
        return "index";
    }
}
