package com.zcx.springsecurity2.controller;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author:
 * @DATE:2023/6/19 11:18
 * @Description:
 * @Version 1.0
 */
@RestController
public class HellowController {
    @RequestMapping("/hello")
    public String hello(){
        System.out.println("hello Security");
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        User user= (User) authentication.getPrincipal();
        System.out.println("身份信息："+user.getUsername());
        System.out.println("权限信息："+authentication.getAuthorities());
        new Thread(()->{
            Authentication authentication1=SecurityContextHolder.getContext().getAuthentication();
            System.out.println(authentication1);
        }).start();
        return "hello ";
    }
}
