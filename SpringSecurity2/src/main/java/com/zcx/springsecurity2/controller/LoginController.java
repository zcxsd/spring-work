package com.zcx.springsecurity2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @Author:
 * @DATE:2023/6/19 13:15
 * @Description:
 * @Version 1.0
 */
@Controller
public class LoginController {
    @RequestMapping("/login.html")
    public String login(){
        return "login";
    }
}
